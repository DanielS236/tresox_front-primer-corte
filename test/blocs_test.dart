import 'package:flutter_test/flutter_test.dart';
import 'package:treox/src/bloc/ia_bloc.dart';
import 'package:treox/src/bloc/tapped_bloc.dart';
import 'package:treox/src/bloc/winner_bloc.dart';
import 'package:treox/src/models/board.dart';

void main(){
  
  test('Movimiento de la IA', () async{
    var bm = BoardModel(0, '', 0, true);
    var result = await IaBloc().iaTurn(bm);
    expect(result, bm);
  });

  test('Movimiento del jugador', () async{
    var index = 0;
    var bm = BoardModel(index, '', 0, true);
    var result = await TappedBloc().tapped(index, bm);
    expect(result.displayBoard[index], 'o');
  });

  test('Movimiento del jugador', () async{
    var index = 0;
    var bm = BoardModel(index, '', 0, true);
    var result = await TappedBloc().tapped(index, bm);
    expect(result.displayBoard[index], 'o');
  });

  test("Revisión de ganador - (Ganando el jugador 'O')", () async{
    var bm = BoardModel(0, 'x', 0, true);
    bm.displayBoard[1] = 'x';
    bm.displayBoard[2] = 'x';
    var result = await WinnerBloc().checkWinner(bm);
    expect(result, 'x');
  });

  test('Revisión de ganador - (empate)', () async{
    var bm = BoardModel(0, '', 9, true);
    var result = await WinnerBloc().checkWinner(bm);
    expect(result, 'Empate');
  });
}