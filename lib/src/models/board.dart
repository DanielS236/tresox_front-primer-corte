// Tablero
class BoardModel {

  // Tablero de juego (las cajas)
  List<String> displayBoard = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
  ];

  // La cantidad de cajas llenas
  int filledBoxes;

  // Turno de la "O" (persona)
  bool oTurn;

  BoardModel(int index, String value, int filled, bool ohTurn){
    displayBoard[index] = value;
    filledBoxes = filled;
    oTurn = ohTurn;
  }

  

}