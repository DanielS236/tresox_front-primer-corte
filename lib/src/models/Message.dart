// Mensaje
class MessageModel {

  String message;
  String type;
  String color;

  MessageModel({
    this.message,
    this.type,
    this.color,
  });

}