import 'package:treox/src/models/board.dart';

class WinnerBloc {

  // Este método se llama desde la actividad de juego cuando el jugador completa su turno
  Future<String> checkWinner(BoardModel boardModel) async{

    // Revisar la fila 1
    if (boardModel.displayBoard[0] == boardModel.displayBoard[1] &&
        boardModel.displayBoard[0] == boardModel.displayBoard[2] &&
        boardModel.displayBoard[0] != '') {
      return (boardModel.displayBoard[0]);
    }

    // Revisar la fila 2
    if (boardModel.displayBoard[3] == boardModel.displayBoard[4] &&
        boardModel.displayBoard[3] == boardModel.displayBoard[5] &&
        boardModel.displayBoard[3] != '') {
      return (boardModel.displayBoard[3]);
    }

    // Revisar la fila 3
    if (boardModel.displayBoard[6] == boardModel.displayBoard[7] &&
        boardModel.displayBoard[6] == boardModel.displayBoard[8] &&
        boardModel.displayBoard[6] != '') {
      return (boardModel.displayBoard[6]);
    }

    // Revisa la columna 1
    if (boardModel.displayBoard[0] == boardModel.displayBoard[3] &&
        boardModel.displayBoard[0] == boardModel.displayBoard[6] &&
        boardModel.displayBoard[0] != '') {
      return (boardModel.displayBoard[0]);
    }

    // Revisa la columna 2
    if (boardModel.displayBoard[1] == boardModel.displayBoard[4] &&
        boardModel.displayBoard[1] == boardModel.displayBoard[7] &&
        boardModel.displayBoard[1] != '') {
      return (boardModel.displayBoard[1]);
    }

    // Revisa la columna 3
    if (boardModel.displayBoard[2] == boardModel.displayBoard[5] &&
        boardModel.displayBoard[2] == boardModel.displayBoard[8] &&
        boardModel.displayBoard[2] != '') {
      return (boardModel.displayBoard[2]);
    }

    // Revisa la diagonal 1
    if (boardModel.displayBoard[6] == boardModel.displayBoard[4] &&
        boardModel.displayBoard[6] == boardModel.displayBoard[2] &&
        boardModel.displayBoard[6] != '') {
      return (boardModel.displayBoard[6]);
    }

    // Revisa la diagonal 2
    if (boardModel.displayBoard[0] == boardModel.displayBoard[4] &&
        boardModel.displayBoard[0] == boardModel.displayBoard[8] &&
        boardModel.displayBoard[0] != '') {
      return (boardModel.displayBoard[0]);
    }

    // Si todas las cajas están llenas, se retorna un empate
    else if(boardModel.filledBoxes == 9){
      return 'Empate';
    }
  return null;
  }
}