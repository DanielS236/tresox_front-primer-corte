import 'package:treox/src/models/board.dart';

class TappedBloc {
// Este método es llamado desde la actividad de juego cada vez que el jugador toca sobre una casilla
  Future<BoardModel> tapped(int index, BoardModel boardModel) async {
    // Revisa de quien es el turno para poner "X" o "O" en el tablero
    if (boardModel.oTurn && boardModel.displayBoard[index] == '') {
      boardModel.displayBoard[index] = 'o';
      boardModel.filledBoxes += 1;

      //Aquí otorga el turno al otro jugador
      boardModel.oTurn = !boardModel.oTurn;
    }

    //Se retorna el modelo del tablero de juego
    return boardModel;
  }
}
