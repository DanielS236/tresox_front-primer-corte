import 'dart:math';

import 'package:treox/src/models/board.dart';

class IaBloc {
  Future<BoardModel> iaTurn(BoardModel boardModel) async{
    var rng = Random();
    int index;

    // Mientras sea el turno de la IA, ejecute lo siguiente
    while (boardModel.oTurn == false) {

      // Genera un numero random que será la posición de la "X" IA
      index = rng.nextInt(9);

      // Revisa si la caja en la posición generada está vacía
      if (boardModel.displayBoard[index] == '') {
        boardModel.displayBoard[index] = 'x';
        boardModel.filledBoxes += 1;
        print(boardModel.filledBoxes);
        //Aquí otorga el turno al otro jugador
        boardModel.oTurn = !boardModel.oTurn;
      }
    }

    return boardModel;
  }
}
