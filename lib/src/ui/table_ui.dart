import 'package:flutter/material.dart';
import 'package:treox/src/bloc/ia_bloc.dart';
import 'package:treox/src/bloc/tapped_bloc.dart';
import 'package:treox/src/bloc/winner_bloc.dart';
import 'package:treox/src/constants.dart';
import 'package:treox/src/models/ScoreModel.dart';
import 'package:treox/src/models/board.dart';

class TableUi extends StatefulWidget {
  @override
  _TableUiState createState() => _TableUiState();
}

class _TableUiState extends State<TableUi> {
  BoardModel boardModel = BoardModel(0, '', 0, true);
  IaBloc iaBloc;
  TappedBloc tappedBloc;
  WinnerBloc winnerBloc;
  ScoreModel scoreModel;
  @override
  void initState() {
    iaBloc = IaBloc();
    tappedBloc = TappedBloc();
    winnerBloc = WinnerBloc();
    scoreModel = ScoreModel(0, 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            RichText(
                text: TextSpan(
                    text: 'O: ' + scoreModel.playerScore.toString(),
                    style: TextStyle(
                        fontSize: 23.0, fontWeight: FontWeight.w600))),
            RichText(
                text: TextSpan(
                    text: 'X: ' + scoreModel.iaScore.toString(),
                    style: TextStyle(
                        fontSize: 23.0, fontWeight: FontWeight.w600))),
          ],
        ),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 15,
          ),
          Expanded(
            flex: 3,
            child: GridView.builder(
                itemCount: 9,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3),
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      movimiento(index);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.lightBlueAccent,
                          border: Border.all(color: Colors.blueAccent)),
                      child: Center(
                        child: Text(
                          boardModel.displayBoard[index],
                          style: TextStyle(
                              color: boardModel.oTurn ? morado : naranja,
                              fontSize: 50,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  void turnoIa(BoardModel boardModel) async {
    this.boardModel = await iaBloc.iaTurn(boardModel);
  }

  void tocado(index, BoardModel bmodel) async {
    boardModel = await tappedBloc.tapped(index, bmodel);
    setState(() {
      boardModel.displayBoard[index];
    });
  }

  void checkWinner(BoardModel boardModel) async {
    var result = await winnerBloc.checkWinner(boardModel);
    if (result != 'Empate' && result != null) {
      showWinDialog(context, result, boardModel);
    } else if (result == 'Empate') {
      clearBoard(boardModel);
    }
  }

  void movimiento(int index) {
    if (boardModel.oTurn) {
      tocado(index, boardModel);
    }

    if (!boardModel.oTurn && boardModel.filledBoxes < 9) {
      turnoIa(boardModel);
    }
    checkWinner(boardModel);
    print(
        '${boardModel.displayBoard} Filled boxes: ${boardModel.filledBoxes} Turno: ${boardModel.oTurn}');
  }

  void showWinDialog(
      BuildContext context, String winner, BoardModel boardModel) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('El ganador es: ' + winner),
            actions: <Widget>[
              ElevatedButton(
                onPressed: () {
                  clearBoard(boardModel);
                  Navigator.of(context).pop();
                },
                child: Text('Jugar de nuevo'),
              )
            ],
          );
        });

    if (winner == 'o') {
      scoreModel.playerScore += 1;
    } else if (winner == 'x') {
      scoreModel.iaScore += 1;
    }
  }

  void clearBoard(BoardModel boardModel) {
    setState(() {
      for (var i = 0; i < 9; i++) {
        boardModel.displayBoard[i] = '';
      }
    });
    boardModel.filledBoxes = 0;
    boardModel.oTurn = true;
  }
}
